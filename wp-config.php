<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'plus1000ch');

/** MySQL database username */
define('DB_USER', 'plus1000');

/** MySQL database password and hostname */
if ($_SERVER['SERVER_ADDR'] == "127.0.0.1") { /* As above */
	define('DB_PASSWORD', 'password');
	define('DB_HOST', 'localhost');
}
else { /* Original values */
	define('DB_PASSWORD', 'earthsnakedesertgone');
	define('DB_HOST', 'mysql.plus1000.ch');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm4nvRPz=7NM@Mh6#(4K,);LjrZQa2~459?SkAP!f|QEq *`TIThG%}bC@,+1!fxp');
define('SECURE_AUTH_KEY',  'p!Q0~FD67E2|lS3Y612_worJIeK=z!|Y_7ImV1<J-lp?6FYTyd%wXUix2Vx[E|jB');
define('LOGGED_IN_KEY',    'hW5xc)u|4X%?CMc(tOURyN<+(-_<]t t]5B9NB6--<NFIraUn ~W;+P_vBKA.k2u');
define('NONCE_KEY',        'Ia~4pS)mMIURmg%iqzJlG2gE;`2Aoax^b{Y[b(t~=#-!PTff${PYN.JDlsO9(j)l');
define('AUTH_SALT',        'VF#z`YU8 AA8+-MC-Dz3ko2#<[[>BuoZMwhzs-v2{GL+FFV-eUr5bSx9].A Mv3Q');
define('SECURE_AUTH_SALT', 'hCy&<pA#d`Jx+A+J`V-VvJ=*2.og +/yLbXW&3?=M.z$aA#--CbIu|S@]5^KfYU-');
define('LOGGED_IN_SALT',   'W9v&(5}2nA}?vxHqM%I!k%/iPUH++!q-+2DHDrw^Lz3[}j18IL;QXbEST$*j?us?');
define('NONCE_SALT',       '%0Pzxm:_n~UGgr(Ny[%^SO{|B=!e_EgKCqz!OY|t|p%?8]^XlDb!~(DZ^0=a%!T$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('MEO_DEBUG', $_SERVER['SERVER_ADDR'] == "127.0.0.1");

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

