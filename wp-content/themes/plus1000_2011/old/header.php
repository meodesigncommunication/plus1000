<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

	<title><?php bloginfo('name');
		if ( is_single() )
			_e('&raquo; Blog Archive');
		wp_title();
	?></title>

<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<style type="text/css" media="screen">
	body {
		background: url("<?php bloginfo('stylesheet_directory'); ?>/images/bg.gif");
		}

#headerimg {
	margin: 0;
	height: 400px;
	width: 900px;
    background: url("<?php bloginfo('stylesheet_directory'); ?>/images/header.png");
	
	}


	#footer {
		background: url("<?php bloginfo('stylesheet_directory'); ?>/images/bg_footer.png") no-repeat center;
		}
</style>

<!--[if lt IE 7]>
	<style type="text/css" media="screen, tv, projection">
		body { behavior: url("<?php bloginfo('stylesheet_directory'); ?>/code/csshover.htc"); }
	</style>
<![endif]-->

<?php wp_head(); ?>
</head>
<body>
<div id="page">

<div id="header">
	<div id="headerimg">
	</div>
</div>