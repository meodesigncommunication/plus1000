<?php
/**
 * @package WordPress
 * @subpackage plus1000_2011_Theme
 */
 /*
Template Name: Agregateur
*/

get_header();

get_sidebar();
?>

<div id="content" class="narrowcolumn">

<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<b>agregateur.php</b><hr />
<?php
}
?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
$myid = get_the_id();
?>

<?php
	$lengthOfExcerpt = 5; // 40
	$readMore = 'hi mum'; // read more text.  to translate.

        $myposts = get_posts("post_parent=".$myid."&post_type=page&numberposts=-1&orderby=menu_order&order=ASC");
         foreach($myposts as $post) :
         setup_postdata($post);
         $attachments =& get_children( 'post_type=attachment&post_mime_type=image&post_parent='.get_the_id());
         if(empty($attachments)) { ?>
         <div class="postWithoutImg">
            <div id="excerptTitle"><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><h2></div>
            <div id="theExcerpt"><? the_advanced_excerpt('length='.$lengthOfExcerpt.'&use_words=1&add_link=1&read_more='.$readMore); ?></div>
        </div><div class="postSeparator"></div>
        <?php }else{
                $imgPrincipale = false;
                foreach($attachments as $attachment => $attachment_array) {
                    $imagearray = wp_get_attachment_image_src($attachment, 'thumbnail', false);
                    $imageURI = $imagearray[0];
                    $imageID = get_post($attachment);
                    $imageTitle = $imageID->post_title;
                    $imageDescription = $imageID->post_content;
                    $imageDescription2 = sanitize_title($imageDescription);
                    if($imageDescription2 == 'image-principale'){
                            $imgPrincipale =true;
                            $uriImgPrincipale = $imageURI;
                            $titleImgPrincipale = $imageTitle;
                            break;
                    }
                }
                ?>  <?php
                if ($imgPrincipale){ ?>
                    <div class="post"><a href="<?php the_permalink(); ?>">
                    <?php echo "<img src=\"".$uriImgPrincipale."\" alt=\"".$titleImgPrincipale."\" class=\"intro_post_image\" />"; ?></a>
                <?php }else{ ?>
                    <div class="postWithoutImg">
                <?php } ?>
            <div id="excerptTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
            <div id="theExcerpt"><? the_advanced_excerpt('length='.$lengthOfExcerpt.'&use_words=1&add_link=1&read_more='.$readMore); ?></div><?php
                if ($imgPrincipale){ ?><div class="leftFloatClearer">&nbsp;</div><?php } ?></div><div class="postSeparator"></div>
        <?php }?>
        
        <?php if( function_exists('ADDTOANY_SHARE_SAVE_BUTTON') ) { ADDTOANY_SHARE_SAVE_BUTTON(); } ?>
        
        <?php endforeach; ?>

    <?php endwhile; else: ?>
    <?php endif; ?>
</div>


<?php
get_footer();
?>
