<?php get_header(); ?>

<?php get_sidebar(); ?>

<?php
	$thumbnails_by_language = array
	(
		'fr' => array
		(
			array
			(
				'image' => 'vignette_tableronde_17sept_FR_web.jpg',
				'link' => 'http://www.plus1000.ch/fr/panel-discussion-september-17-4pm/'
			),
			array
			(
				'image' => 'vignette_ami-du-festival.jpg',
				'link' => 'http://www.plus1000.ch/fr/friends-of-the-festival-alt-1000/'
			),
			array
			(
				'image' => '3_vignette_facebook_web.jpg',
				'link' => 'https://www.facebook.com/pages/Alt-1000-High-Altitude-photography-in-the-mountains/148109168579661'
			)
		),
		'en' => array
		(
			array
			(
				'image' => 'vignette_tableronde_17sept_EN_web.jpg',
				'link' => 'http://www.plus1000.ch/panel-discussion-september-17-4pm/'
			),
			array
			(
				'image' => 'vignette_ami-du-festivalEN.jpg',
				'link' => 'http://www.plus1000.ch/friends-of-the-festival-alt-1000/'
			),
			array
			(
				'image' => '3_vignette_facebook_web_EN.jpg',
				'link' => 'https://www.facebook.com/pages/Alt-1000-High-Altitude-photography-in-the-mountains/148109168579661'
			)
		)
	);

?>
	<div id="content" class="narrowcolumn">
<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<b>home.php</b><hr />
<?php
}


// Format for qTranslate plugin. urlencode so Advanced Excerpt will not convert +s to space
$READ_MORE_TEXT = urlencode (__("<!--:en-->+ read more<!--:--><!--:fr-->+ lire plus<!--:-->"));

$LEADING_CATEGORY = 'welcome';

$do_not_duplicate = array();

// Split into two as I can't see an obvious way to stop the_advanced_excerpt() echoing to the screen
function showPostStart($postId, $permalink, $title, $firstpost = 0) {
	$imageDetails = null;
	if ( function_exists('meo_get_attached_image_with_description') ) {
		$imageDetails = meo_get_attached_image_with_description($postId, 'image-principale', $firstpost ? 'large' : 'thumbnail');
	}

	// First post has a full size image
	if ($firstpost and !is_null($imageDetails)) { ?>
		<img src="<?php echo $imageDetails['url']; ?>" alt="<?php echo $imageDetails['title']; ?>" class="intro_post_image" /><br />
		<?php if (!is_null($imageDetails['caption'])) { ?>
			<div class="photographer"><cite><?php echo $imageDetails['caption']; ?></cite></div><div class="rightFloatClearer">&nbsp;</div>
		<?php } ?>
	<?php }

	$postWithoutImage = $firstpost || is_null ($imageDetails);

	if ($postWithoutImage) { ?>
		<div class="post">
	<?php } else { ?>
		<div class="postWithThumb">
			<div class="intro_post_image">
				<img src="<?php echo $imageDetails['url']; ?>" alt="<?php echo $imageDetails['title']; ?>" />
				<?php if (!empty($imageDetails['caption'])) { ?>
					<br/><cite><?php echo $imageDetails['caption']; ?></cite>
				<?php } ?>
			</div>
	<?php } ?>
	<div id="excerptTitle"><h2><?php echo $title; ?></h2><?php if (!$firstpost) { ?><div class="postdate"><?php the_date(); ?></div><?php } ?></div>
	<div id="theExcerpt">
	<?php

	return $postWithoutImage;
}

function showPostEnd($postWithoutImage, $permalink) {
	echo meo_getShareLinks($permalink);

	if (!$postWithoutImage) {
		?><div class="leftFloatClearer">&nbsp;</div><?php
	} ?>
	</div><div class="postSeparator"></div></div><?php
}

$firstpost = 1;

// Save the original query (see http://codex.wordpress.org/The_Loop#Multiple_Loops_Example_2)
$original_query = clone $wp_query;

query_posts('category_name=' . $LEADING_CATEGORY);

if ( have_posts() ) : while (have_posts()) : the_post();
	$do_not_duplicate[] = $post->ID;
	$postWithoutImage = showPostStart($post->ID
	                                , get_permalink($post->ID)
	                                , the_title('', '', false)
     	                            , $firstpost);

	// Try to take the whole article (10,000 words), or until a break specified in the markup
    the_advanced_excerpt('length=10000&use_words=1&add_link=1&read_more='.$READ_MORE_TEXT);

    showPostEnd($postWithoutImage
              , get_permalink($post->ID));

	$firstpost = 0;
endwhile;
endif;

// Restore original query for processing
$wp_query = clone $original_query;

// Process remaining articles
if ( have_posts() ) : while ( have_posts() ) : the_post();

	// Ensure we don't show anything from the leading category
	if (in_array($post->ID, $do_not_duplicate)) {
		continue;
	}

	setup_postdata($post);
	$postWithoutImage = showPostStart($post->ID
	                                , get_permalink($post->ID)
	                                , the_title('', '', false)
     	                            , $firstpost);

	echo '<p>';
	// Exclude <p> as we want to do manually, including the read more link if necessary
	// Takes the length and use words values from the admin screen
	the_advanced_excerpt('add_link=1&read_more='.$READ_MORE_TEXT.'&exclude_tags=p');

	if (preg_match('/<!--more(.*?)?-->/', $post->post_content)) {
		echo '&#8230; <a href="' . get_permalink($post->ID) . "\" class=\"more-link\">" . urldecode(__($READ_MORE_TEXT)) . "</a>";
	}
	echo '</p>';

	showPostEnd($postWithoutImage, get_permalink($post->ID));

	$firstpost = 0;
endwhile; ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> <!--:en-->Older posts<!--:--><!--:fr-->articles pr&eacute;c&eacute;dents<!--:-->', 'twentyten' ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( '<!--:en-->Newer posts<!--:--><!--:fr-->articles suivants<!--:--> <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
				</div><!-- #nav-below -->
<?php endif; ?>

<?php else: ?>
    	<!-- No matching pages found  -->
<?php endif; ?>
</div>


<?php
get_footer();
?>

