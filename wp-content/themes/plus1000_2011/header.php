<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script>
<?php
}
?>
<!-- Infomaniak -->
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<?php
//    wp_enqueue_script('fbshare', 'http://static.ak.fbcdn.net/connect.php/js/FB.Share', array(), false);
?>


	<title><?php bloginfo('name');
		if ( is_single() )
			_e('&raquo; Blog Archive');
		wp_title();
	?></title>

<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>">

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<!--[if lt IE 7]>
	<style type="text/css" media="screen, tv, projection">
		body { behavior: url("<?php bloginfo('stylesheet_directory'); ?>/code/csshover.htc"); }
	</style>
<![endif]-->


<!--[if !IE]><!-->
<style type="text/css">
body {
    font-size:10px;
}
</style>
<!--<![endif]-->


<?php wp_head(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22663733-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body <?php body_class('lang-' . meo_getCurrentLanguageISOCode()); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?php echo meo_getCurrentLocale(); ?>/all.js#xfbml=1&appId=576222319110542";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="page">

<div id="header"></div>