<?php
/**
 * @package WordPress
 * @subpackage plus1000_2011_Theme
 */
 /*
Template Name: Press Download
*/

get_header();

function read_directory($directory_name, $ignore = array( '.', '..' )) {
	$result = array();
	if ($handle = opendir($directory_name)) {
	    while (false !== ($file = readdir($handle))) {
	    	if( !in_array( $file, $ignore ) ){
	    		$result[] = $file;
	    	}
	    }

	    closedir($handle);

		sort($result);
	}

	return $result;
}

function escape_characters($string, $type = 'html') {
	$result = '';
	$chars = str_split($string);
	foreach($chars as $char) {
		if (ord($char) > 127) { ## Special character
			if ($type == 'html') {
				$result .= "&#".ord($char).";";
			}
			elseif ($type == 'url') {
				$result .= "%".dechex(ord($char));
			}
			else {
				$result .= $char;
			}
		}
		else {
			$result .= $char;
		}
	}
	return $result;
}

function get_thumb($image) {
	## Replace copyright symbol with _, and append "_small" to filename
	##   ANNE_GOLAZ_Le_chasseur_de_chamois©2010.jpg
	## becomes
	##   ANNE_GOLAZ_Le_chasseur_de_chamois_2010_small
	return preg_replace('/^(.*)\xA9(.*)\.jpg$/', 'small/${1}_${2}_small.jpg', $image);
}

function frenchUpper($string){
	// Handle french characters when uppercasing a string
	return strtr(strtoupper($string), array(
		chr(224) => 'A',
		chr(192) => 'A',
		chr(232) => 'E',
		chr(200) => 'E',
		chr(236) => 'I',
		chr(204) => 'I',
		chr(242) => 'O',
		chr(210) => 'O',
		chr(249) => 'U',
		chr(217) => 'U',
		chr(225) => 'A',
		chr(193) => 'A',
		chr(233) => 'E',
		chr(201) => 'E',
		chr(237) => 'I',
		chr(205) => 'I',
		chr(243) => 'O',
		chr(211) => 'O',
		chr(250) => 'U',
		chr(218) => 'U',
		chr(226) => 'A',
		chr(194) => 'A',
		chr(234) => 'E',
		chr(202) => 'E',
		chr(238) => 'I',
		chr(206) => 'I',
		chr(244) => 'O',
		chr(212) => 'O',
		chr(251) => 'U',
		chr(219) => 'U',
		chr(231) => 'C',
		chr(199) => 'C',
	));
}

function format_image_name($artist, $image) {
	$result = $image;
	if (strncasecmp(frenchUpper($artist), frenchUpper($image), strlen($artist)) == 0) {
		// If the image name starts with the artist name, strip it
		$result = substr($image, strlen($artist));
	}
	$result = str_replace ( '_', ' ', $result);
	$result = preg_replace('/^(.*)\xA9(.*)\.jpg$/', '${1} ' . chr(169) . ' ${2}', $result);
	return escape_characters($result);
}

?>

<?php get_sidebar(); ?>

<div id="content" class="narrowcolumn pressdownload">

<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<b>press_download_listing.php</b><hr />
<?php
}
?>


<table>

<?php
$press_root =  WP_CONTENT_DIR . '/press';

$artists = read_directory($press_root);

foreach( $artists as $artist) {
	$artist_output = str_replace ( '_', ' ', $artist); ?>
	<tr><td colspan="4"><h2><?php echo escape_characters($artist_output); ?></h2></td></tr>
	<tr><td colspan="4" class="linktext"><?php _e('<!--:en-->To download, right click on image and choose "Save As..."<!--:--><!--:fr-->Pour t&eacute;l&eacute;charger, clic droit sur l\'image et choisissez "Enregistrer sous ..."<!--:-->'); ?></td></tr>
	<tr>
	<?php
	$index = 0;
	$images = read_directory($press_root . '/' . $artist, array( '.', '..', '.DS_Store', 'Thumbs.db', 'small' ));
	foreach ($images as $image) {
		if ($index!= 0 && $index % 4 == 0) {
			?></tr><tr><?php
		}
		echo '<td class="artist_image">';
		echo '<a href="' . WP_CONTENT_URL . '/press/' . escape_characters($artist, 'url') . '/' . escape_characters($image, 'url') . '" >';
		echo '<img src="' . WP_CONTENT_URL . '/press/' . escape_characters($artist, 'url') . '/' . escape_characters(get_thumb($image), 'url') . '" />';
		echo '<cite>' . format_image_name($artist, $image) . '</cite><br />';
		echo '</a>';
		echo '</td>';
		$index++;
	}
	if ($index % 4 != 0) {
		?><td colspan="<?php echo 4 - ($index % 4); ?>">&nbsp;</td><?php
	}
	?></tr><?php
}

?>
</table>

</div><!-- #content -->

<?php
get_footer();
?>

