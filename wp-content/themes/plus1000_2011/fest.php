<?php
/*
Template Name: Page interieur
*/
?>
<?php include (TEMPLATEPATH . '/header_fest.php'); ?>
<?php wp_head(); ?>
<?php get_sidebar(); ?>

<div id="content" class="narrowcolumn">

<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<b>fest.php</b><hr />
<?php
}
?>


    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entrytext">
				<?php the_content('<p class="serif">' . __('Read the rest of this page &raquo;') . '</p>'); ?>
			<?php if (function_exists('gengo_link_pages')) : ?>
					<?php gengo_link_pages('<p><strong>' . __('Pages:') . '</strong> ', '</p>', 'number'); ?>
			<?php else : ?>
					<?php link_pages('<p><strong>' . __('Pages:') . '</strong> ', '</p>', 'number'); ?>
			<?php endif; ?>
			</div>
		</div>
	  <?php endwhile; endif; ?>
	<?php edit_post_link(__('Edit this entry.'), '<p>', '</p>'); ?>
	</div>

<?php get_footer(); ?>