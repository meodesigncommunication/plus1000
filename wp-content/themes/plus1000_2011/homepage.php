<?php
/**
 * Template name: Homepage NEW TPL
 */


    $args = array(
        'posts_per_page'   => 3,
        'post_type'        => 'post'
    );

    $recent_posts = get_posts( $args );

?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

    <div id="content" class="narrowcolumn">

        <?php
        if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
            ?>
            <b>homepage.php</b><hr />
            <?php
        }
        ?>



        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <h2><?php the_title(); ?></h2>
                <div class="entrytext">
                    
                    <div>
                        <?php the_content('<p class="serif">' . __('Read the rest of this page &raquo;') . '</p>'); ?>
                        <?php if (function_exists('gengo_link_pages')) : ?>
                            <?php gengo_link_pages('<p><strong>' . __('Pages:') . '</strong> ', '</p>', 'number'); ?>
                        <?php else : ?>
                            <?php link_pages('<p><strong>' . __('Pages:') . '</strong> ', '</p>', 'number'); ?>
                        <?php endif; ?>
                    </div>

                    <div style="clear:both">&nbsp;</div>

                    <div id="widget_area">
                        <?php //if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('recent_post_homepage') ) ?>
                        <div class="postSeparator"></div>
                        <?php foreach($recent_posts as $recent_post): ?>
                            <div class="postWithThumb">
                                <div class="intro_post_image">
                                    <?php
                                        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($recent_post->ID));
                                    ?>
                                    <img src="<?php echo $thumbnail[0]; ?>" alt="<?php echo __($recent_post->post_title) ?>">
                                </div>
                                <div id="excerptTitle">
                                    <h2><?php echo __($recent_post->post_title) ?></h2>
                                    <div class="postdate"><?php $pfx_date = get_the_date( 'F j, Y', $recent_post->ID ); ?></div>
                                </div>
                                <div id="theExcerpt">
                                    <p></p>
                                    <p>
                                        Découvrez notre dernière Newsletter!…
                                        <a class="read_more" href="<?php echo $recent_post->guid ?>">+ lire plus</a>
                                    </p>
                                    <p></p>
                                    <div class="addthis_toolbox addthis_default_style" addthis:url="http://www.plus1000.ch/fr/newsletter-6-october-2015/" addthis:ui_language="fr">
                                        <a class="addthis_button_email" style="cursor:pointer;" title="email" target="_blank" href="#">
                                            <img src="http://www.plus1000.ch/wp-content/themes/plus1000_2011/images/mail.png" alt="email" border="0">
                                            email
                                        </a>
                                        <a class="addthis_button_facebook" style="cursor:pointer;" title="partager sur facebook" href="#">
                                            <img src="http://www.plus1000.ch/wp-content/themes/plus1000_2011/images/facebook-icon.png" alt="facebook" border="0">
                                            partager sur facebook
                                        </a>
                                        <div class="atclear"></div>
                                    </div>
                                    <div class="leftFloatClearer"> </div>
                                </div>
                                <div class="postSeparator"></div>
                            </div>
                        <?php endforeach; ?>

                    </div>

                </div>
            </div>
        <?php endwhile; endif; ?>
        <?php edit_post_link(__('Edit this entry.'), '<p>', '</p>'); ?>

    </div>

<?php get_footer(); ?>