<?php

add_theme_support( 'post-thumbnails' );

if ( function_exists('register_sidebars') )
	    register_sidebar(array(
	        'before_widget' => '<li>',
	        'after_widget' => '</li>',
	        'before_title' => '<h2>',
        	'after_title' => '</h2>',
    	));

function widget_jillij_search() {
?>

			<li class="LargeMenu"><h2><?php _e('Search'); ?></h2>
						<ul>
							<li><?php include (TEMPLATEPATH . "/searchform.php"); ?>
							</li>
						</ul>
			</li>

<?php
}

function widget_jillij_calendar() {
?>

			<li class="LargeMenu"><h2><?php _e('Archives'); ?></h2>
				<ul>
					<li>
					<?php get_calendar(); ?>
					</li>
				</ul>
			</li>


<?php
}

function widget_jillij_categories($args) {
		extract($args);
		$options = get_option('widget_categories');
		$c = $options['count'] ? '1' : '0';
		$h = $options['hierarchical'] ? '1' : '0';
		$title = empty($options['title']) ? __('Categories') : $options['title'];
?>
			<li><h2><?php echo $title; ?></h2>
				<ul>
				<?php wp_list_cats('sort_column=name&optioncount=$c&hierarchical=$h'); ?>
				</ul>
			</li>


<?php
}

function widget_jillij_gsearch($args) {

		// $args is an array of strings that help widgets to conform to
		// the active theme: before_widget, before_title, after_widget,
		// and after_title are the array keys. Default tags: li and h2.
		extract($args);

		// Each widget can store its own options. We keep strings here.
		$options = get_option('widget_gsearch');
		$title = $options['title'];
		$buttontext = $options['buttontext'];

		// These lines generate our output. Widgets can be very complex
		// but as you can see here, they can also be very, very simple.
		echo '<li class="LargeMenu">'.$before_title .__('Search'). $after_title.'<ul><li>';
		$url_parts = parse_url(get_bloginfo('home'));
		echo '<form id="searchform" action="http://www.google.com/search" method="get" onsubmit="this.q.value=\'site:'.$url_parts['host'].' \'+this.rawq.value"><input name="rawq" size="20" /><input type="hidden" name="q" value="" /><input value="'.$buttontext.'" name="submit" type="submit" /></form>';
		echo '</li></ul>'.$after_widget;
	}

function widget_text_jillij($args, $number = 1) {
	extract($args);
	$options = get_option('widget_text');
	$title = $options[$number]['title'];
	if ( empty($title) )
		$title = '&nbsp;';
	$text = $options[$number]['text'];
?>
		<li class="LargeMenu">
		<?php $title ? print($before_title . $title . $after_title) : null; ?>
			<ul>
			<li><?php echo $text; ?></li>
			</ul>
		<?php echo $after_widget; ?>
<?php
}

function widget_text_register_jillij() {
	$options = get_option('widget_text');
	$number = $options['number'];
	if ( $number < 1 ) $number = 1;
	if ( $number > 9 ) $number = 9;
	for ($i = 1; $i <= 9; $i++) {
		$name = array('Text %s', null, $i);
		register_sidebar_widget($name, $i <= $number ? 'widget_text_jillij' : /* unregister */ '', $i);
	}
}

function widget_jillij_polyglot($args) {
	extract($args);
	$options = get_option('widget_polyglot');
	$title = $options['title'];
	$listtype = $options['listtype'] ? true : false ;

	echo polyglot_list_langs($listtype);
	}

function widget_jillij_subpagehierarchy($args) {
	// $args is an array of strings that help widgets to conform to
	// the active theme: before_widget, before_title, after_widget,
	// and after_title are the array keys. Default tags: li and h2.
	extract($args);

	// Each widget can store its own options. We keep strings here.
	$options = get_option('widget_subpagehierarchy');
	$title = $options['title'];
	$headpage = $options['headpage'];
	settype($headpage,"integer");
	// These lines generate our output. Widgets can be very complex
	// but as you can see here, they can also be very, very simple.
	echo $before_widget . $before_title . $title . $after_title;
	echo "<ul>";
	wp_list_pages("sort_column=menu_order&child_of=$headpage&title_li=" );
	echo "</ul>";
	echo $after_widget;
	}

if ( function_exists('register_sidebar_widget') ) {
    register_sidebar_widget(__('Search'), 'widget_jillij_search');
    register_sidebar_widget('Calendar', 'widget_jillij_calendar');
	register_sidebar_widget('Categories', 'widget_jillij_categories');
	if ( function_exists('widget_subpagehierarchy') )
		register_sidebar_widget('Sub page hierarchy', 'widget_jillij_subpagehierarchy');

	widget_text_register_jillij();

	if ( function_exists('widget_polyglot') )
		register_sidebar_widget('Polyglot', 'widget_jillij_polyglot');
	if ( function_exists('widget_gsearch') )
		register_sidebar_widget('Google Search', 'widget_jillij_gsearch');
}


function jillij_admin_header_style() {
?>
<style type="text/css">
#headimg {
	margin: 7px 9px 0;
	height: <?php echo HEADER_IMAGE_HEIGHT; ?>px;
	width: <?php echo HEADER_IMAGE_WIDTH; ?>px;
	background: url(<?php header_image() ?>) no-repeat;
}

#headimg h1 a, #desc, #headimg h1 a:hover, #headimg h1 a:visited {
	color:#<?php header_textcolor() ?>;
}

#headimg h1 {
	padding-top: 45px;
	}

#headimg h1, #headimg h1, #headimg h1 a, #headimg h1 a:hover, #headimg h1 a:visited{
	text-decoration: none;
	font-size: 40px;
	text-align: center;
	font-family: 'Trebuchet MS', 'Lucida Grande', Verdana, Arial, Sans-Serif;
	font-weight: bold;
	}

#desc {
	font-size: 1.2em;
	text-align: center;
	}

</style>
<?php
}
function jillij_header_style() {
?>
<style type="text/css">

<?php if ( 'blank' == get_header_textcolor() ) { ?>

#headerimg h1, .description {
	display: none;
}

<?php } ?>

</style>
<?php
}
if ( function_exists('add_custom_image_header') ) {
	add_custom_image_header('jillij_header_style', 'jillij_admin_header_style');
}

function meo_get_attached_image_with_description($post_id = null, $description = '', $size = 'thumbnail') {
    if ( !$post_id ) {
        return null;
    }

    // Use featured image, if set
    if (has_post_thumbnail($post_id)) {
		$thumb = get_post_thumbnail_id($post_id);
		$imagearray = wp_get_attachment_image_src( $thumb, $size );
		$imagePostDetails = get_post($thumb);

		return array("url"     => $imagearray[0]
		           , "width"   => $imagearray[1]
		           , "height"  => $imagearray[2]
		           , "title"   => $imagePostDetails->post_title
		           , "caption" => $imagePostDetails->post_excerpt );
    }


    $attachments = &get_children( 'post_type=attachment&post_mime_type=image&post_parent='.$post_id);
    if(empty($attachments)) {
        return null;
    }

    foreach($attachments as $attachment => $attachment_array) {
        $imagePostDetails = get_post($attachment);
        $imageDescription = sanitize_title($imagePostDetails->post_content);

        if($imageDescription == $description){
            $imagearray = wp_get_attachment_image_src($attachment, $size, false);
            $titleImg = $imagePostDetails->post_title;

            return array("url"     => $imagearray[0]
                       , "width"   => $imagearray[1]
                       , "height"  => $imagearray[2]
                       , "title"   => $imagePostDetails->post_title
                       , "caption" => $imagePostDetails->post_excerpt );
        }
    }
    return null;
}


// Language Select Code for non-Widget users
function meo_generateLanguageSelectCode($style='', $id='', $display_type='') {
	if($display_type=='' or $display_type=='language_name' or ($display_type != 'locale' and $display_type != 'pre_domain')) {
		// Just use the default.
		return qtrans_generateLanguageSelectCode($style, $id);
	}

	global $q_config;

	if($style=='') $style='text';
	if(is_bool($style)&&$style) $style='image';
	if(is_404()) $url = get_option('home'); else $url = '';
	if($id=='') $id = 'qtranslate';
	$id .= '-chooser';
	switch($style) {
		case 'image':
		case 'text':
		case 'dropdown':
			echo '<ul class="qtrans_language_chooser" id="'.$id.'">';
			foreach(qtrans_getSortedLanguages() as $language) {
				echo '<li';
				if($language == $q_config['language'])
					echo ' class="active"';
				echo '><a href="'.qtrans_convertURL($url, $language).'"';
				// set hreflang
				echo ' hreflang="'.$language.'"';
				if($style=='image')
					echo ' class="qtrans_flag qtrans_flag_'.$language.'"';
				echo '><span';
				if($style=='image')
					echo ' style="display:none"';
				echo '>'.$q_config[$display_type][$language].'</span></a></li>';
			}
			echo "</ul><div class=\"qtrans_widget_end\"></div>";
			if($style=='dropdown') {
				echo "<script type=\"text/javascript\">\n// <![CDATA[\r\n";
				echo "var lc = document.getElementById('".$id."');\n";
				echo "var s = document.createElement('select');\n";
				echo "s.id = 'qtrans_select_".$id."';\n";
				echo "lc.parentNode.insertBefore(s,lc);";
				// create dropdown fields for each language
				foreach(qtrans_getSortedLanguages() as $language) {
					echo qtrans_insertDropDownElement($language, qtrans_convertURL($url, $language), $id);
				}
				// hide html language chooser text
				echo "s.onchange = function() { document.location.href = this.value;}\n";
				echo "lc.style.display='none';\n";
				echo "// ]]>\n</script>\n";
			}
			break;
		case 'both':
			echo '<ul class="qtrans_language_chooser" id="'.$id.'">';
			foreach(qtrans_getSortedLanguages() as $language) {
				echo '<li';
				if($language == $q_config['language'])
					echo ' class="active"';
				echo '><a href="'.qtrans_convertURL($url, $language).'"';
				echo ' class="qtrans_flag_'.$language.' qtrans_flag_and_text"';
				echo '><span>'.$q_config[$display_type][$language].'</span></a></li>';
			}
			echo "</ul><div class=\"qtrans_widget_end\"></div>";
			break;
	}
}


function meo_generateSimpleLanguageSelect($display_type='language_name', $link_on_current_language = true) {

	if($display_type!='language_name' and $display_type != 'locale' and $display_type != 'pre_domain') {
		$display_type='language_name';
	}

    global $q_config;

    $url = '';
    if (is_404()) {
        $url = home_url();
    }

    $languages = qtrans_getSortedLanguages();

    if (count($languages) <= 1) {
    	// No point generating if we only have one language
    	return;
    }


    echo "<div class=\"meoLangSelect\">";

    $first = true;

    foreach($languages as $language) {
    	if (!$first) {
    		echo "<span class=\"meoLangSelectDivider\">|</span>";
    	}
    	else {
    		$first = false;
    	}

    	if ($link_on_current_language or $language != $q_config['language']) {
    		echo '<a href="'.qtrans_convertURL($url, $language).'" hreflang="'.$language.'">';
    	}

    	echo '<span class="meoLangLink">'.$q_config[$display_type][$language].'</span>';

    	if ($link_on_current_language or $language != $q_config['language']) {
    		echo "</a>";
    	}
    }

    echo "</div>";

}


function meo_getCurrentLocale() {
	global $q_config;
	return $q_config['locale'][$q_config['language']];
}

function meo_getCurrentLanguageISOCode() {
	global $q_config;
	return $q_config['pre_domain'][$q_config['language']];
}


function meo_getShareLinks($permalink) {
	$EMAIL_SHARE_TEXT = __("<!--:en-->email<!--:--><!--:fr-->email<!--:-->");
	$FB_SHARE_TEXT = __("<!--:en-->share on facebook<!--:--><!--:fr-->partager sur facebook<!--:-->");

	return "<div class=\"addthis_toolbox addthis_default_style\" addthis:url=\"" . $permalink . "\" addthis:ui_language=\"" . meo_getCurrentLanguageISOCode() ."\">"
	     . "   <a class=\"addthis_button_email\" style=\"cursor:pointer;\" title=\"" . $EMAIL_SHARE_TEXT . "\"><img src=\"" . get_bloginfo('template_url') . "/images/mail.png\" border=\"0\" alt=\"email\" />&nbsp;" . $EMAIL_SHARE_TEXT . "</a>"
	     . "   <a class=\"addthis_button_facebook\" style=\"cursor:pointer;\" title=\"" . $FB_SHARE_TEXT . "\"><img src=\"" . get_bloginfo('template_url') . "/images/facebook-icon.png\" border=\"0\" alt=\"facebook\" />&nbsp;" . $FB_SHARE_TEXT . "</a>"
          . "</div>";
}

/*** --------------------------------------------------------------------- ***
     Enable iframes in the TinyMCE editor
 *** --------------------------------------------------------------------- ***/

add_filter('tiny_mce_before_init', 'meo_tinymce_config');
function meo_tinymce_config( $init ) {
	// Add IFRAME to list of valid HTML elements (so they don't get stripped)
	$valid_iframe = 'iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]';

	// Add to extended_valid_elements if it already exists
	if ( isset( $init['extended_valid_elements'] ) ) {
		$init['extended_valid_elements'] .= ',' . $valid_iframe;
	} else {
		$init['extended_valid_elements'] = $valid_iframe;
	}

	return $init;
}


/*** --------------------------------------------------------------------- ***
     Allow multilingual thumbnails.
     Assumes
       - uploaded in same month
       - english file name contains _EN.
       - french file name contains _FR.
 *** --------------------------------------------------------------------- ***/

add_filter('get_post_metadata', 'meo_translate_thumbnail', 10, 4);
function meo_translate_thumbnail($metadata, $object_id, $meta_key, $single) {
	global $wpdb;

	if(isset($meta_key) && $meta_key == "_thumbnail_id") {
		$to_lang = meo_getCurrentLanguageISOCode();
		$from_lang = ($to_lang == "fr" ? "en" : "fr");

		$sql = "select trthumb.post_id
				  from wp_postmeta orig
				       join wp_postmeta thumb on orig.meta_value = thumb.post_id
				       cross join wp_postmeta trthumb
				 where orig.post_id = " . (int) $object_id . "
				   and orig.meta_key = '_thumbnail_id'
				   and thumb.meta_key = '_wp_attached_file'
				   and trthumb.meta_key = '_wp_attached_file'
				   and trthumb.meta_value = replace(thumb.meta_value, '_" . strtoupper($from_lang) . ".', '_" . strtoupper($to_lang) . ".')";

		$image_id = $wpdb->get_var( $sql );

		if ($image_id) {
			return $image_id;
		}
	}
	return $metadata;
}

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'recent_post_homepage',
        'before_widget' => '<div class="widget_sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
}

?>
