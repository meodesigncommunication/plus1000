<?php
/*
Plugin Name: aplus theme supplements - used for jillij theme
Plugin URI: http://www.aplus.co.yu/deliver/wp/
Description: Required for proper operation of the aplus theme - used here for jillij theme
Author: Aleksandar Vaci�
Version: 1.2
Author URI: http://www.aplus.co.yu/

	##	incorporates (bugfixed) Dunstan s Time Since plugin by Michael Heilemann & Dunstan Orchard.
	##	incorporates Time of day by Phu Ly & Dunstan Orchard & Michael Heilemann

	## special formatting for blog roll, category list

*/

class aplus {

	function cleanthemedir() {
		$cleanstyledir = get_stylesheet_directory_uri();
		$cleanstyledir = substr($cleanstyledir, strpos($cleanstyledir, $_SERVER['SERVER_NAME']));
		$cleanstyledir = substr($cleanstyledir, strpos($cleanstyledir, "/"));
		return $cleanstyledir;
	}

	/*
	 * method list_cats()
	 *
	 * Output an unordered list of all categories with post count
	 *
	 * Parameters:
	 *   order (default 'name')  - Sort link categories by 'name' or 'id'
	 *   hide_if_empty (default true)  - Supress listing empty link categories
	 */
	function list_cats($sort_column = 'ID', $sort_order = 'asc', $child_of = 0, $hierarchical = 0) {
		global $wpdb, $category_posts;

		$hide_empty = 1;
		$use_desc_for_title = 1;

			$sort_column = 'cat_'.$sort_column;

			$query  = "
				SELECT cat_ID, cat_name, category_nicename, category_description, category_parent
				FROM $wpdb->categories
				WHERE cat_ID > 0
				ORDER BY $sort_column $sort_order";

			$categories = $wpdb->get_results($query);

		if (!count($category_posts)) {
			$now = current_time('mysql', 1);
			$cat_counts = $wpdb->get_results("	SELECT cat_ID,
			COUNT($wpdb->post2cat.post_id) AS cat_count
			FROM $wpdb->categories
			INNER JOIN $wpdb->post2cat ON (cat_ID = category_id)
			INNER JOIN $wpdb->posts ON (ID = post_id)
			WHERE post_status = 'publish'
			AND post_date_gmt < '$now'
			GROUP BY category_id");
	        if (! empty($cat_counts)) {
	            foreach ($cat_counts as $cat_count) {
	                if (1 != intval($hide_empty) || $cat_count > 0) {
	                    $category_posts["$cat_count->cat_ID"] = $cat_count->cat_count;
	                }
	            }
	        }
		}

		$num_found=0;
		$thelist = "";

		foreach ($categories as $category) {
			if ((intval($hide_empty) == 0 || isset($category_posts["$category->cat_ID"])) && (1 || $category->category_parent == $child_of) ) {
				$num_found++;
				$link = '<a href="'.get_category_link($category->cat_ID).'" ';
				if (empty($category->category_description)) {
					$link .= 'title="'. sprintf(__("View all posts filed under %s"), wp_specialchars($category->cat_name)) . '"';
				} else {
					$link .= 'title="' . strip_tags(apply_filters('category_description',$category->category_description,$category)) . '"';
				}
				$link .= '>';
				$link .= apply_filters('list_cats', $category->cat_name, $category);
				$link .= ' '.intval($category_posts["$category->cat_ID"]).'</a>';

				$strsubmenu = '';
				if ($hierarchical) $strsubmenu = ' class="submenu"';

				$thelist .= "\t<li$strsubmenu>$link\n";

				if ($hierarchical) $thelist .= aplus::list_cats($sort_column, $sort_order, $category->cat_ID, $hierarchical);
				$thelist .= "</li>\n";
				}
		}
		if (!$num_found && !$child_of){
			$before = '<li>';
			$after = '</li>';
			echo $before . __("No categories") . $after . "\n";
			return;
		}
		if ($child_of && $num_found && $recurse) {
			$pre = '\t<ul id="cat' . $category->cat_ID . 'Menu">';
			$post = "\t</ul>\n";
		} else {
			$pre = $post = '';
		}
		$thelist = $pre . $thelist . $post;
		if ($recurse) {
			return $thelist;
		}
		echo apply_filters('list_cats', $thelist);
	}

	/*
	 * method blog_roll()
	 *
	 * Output an unordered list of all links, listed by category
	 *
	 * Parameters:
	 *   order (default 'name')  - Sort link categories by 'name' or 'id'
	 *   hide_if_empty (default true)  - Supress listing empty link categories
	 */
	function blog_roll($order = 'name', $hide_if_empty = 'obsolete', $showimage = true, $showdesc = true, $showrating = true, $showupdated = true) {
		global $wpdb;

		$order = strtolower($order);

		// Handle link category sorting
		if (substr($order,0,1) == '_') {
			$direction = ' DESC';
			$order = substr($order,1);
		}

		// if 'name' wasn't specified, assume 'id':
		$cat_order = ('name' == $order) ? 'cat_name' : 'cat_id';

		if (!isset($direction)) $direction = '';
		// Fetch the link category data as an array of hashes
		$cats = $wpdb->get_results("
			SELECT DISTINCT link_category, cat_name, show_images,
				show_description, show_rating, show_updated, sort_order,
				sort_desc, list_limit
			FROM `$wpdb->links`
			LEFT JOIN `$wpdb->linkcategories` ON (link_category = cat_id)
			WHERE link_visible =  'Y'
				AND list_limit <> 0
			ORDER BY $cat_order $direction ", ARRAY_A);

		// Display each category
		if ($cats) {
			foreach ($cats as $cat) {
				// Handle each category.
				// First, fix the sort_order info
				$orderby = $cat['sort_order'];
				$orderby = (bool_from_yn($cat['sort_desc']) ? '_' : '') . $orderby;

				$showimage = $showimage && (bool_from_yn($cat['show_images']));
				$showdesc = $showdesc && (bool_from_yn($cat['show_description']));
				$showrating = $showrating && (bool_from_yn($cat['show_rating']));
				$showupdated = $showupdated && (bool_from_yn($cat['show_updated']));

				// Display the category name
				//echo '<li class="submenu" id="linkcat' . $cat['link_category'] . 'Menu"><a href="#">' . $cat['cat_name'] . "</a>\n\t<ul>\n";
				// Call get_links() with all the appropriate params
				get_links($cat['link_category'],
					'<li>',"</li>","\n",
					$showimages,
					$orderby,
					$showdesc,
					$showrating,
					$cat['list_limit'],
					$showupdated);

				// Close the last category
				//echo "\n\t</ul>\n</li>\n";
			}
		}
	}

	/*
	 * method hot_topics()
	 *
	 * get specified number of entries that were recently commented on
	 *	- fetch last X comments
	 * - group by post
	 *
	 *	uses code snipets from plugin by Krischan Jodies (http://blog.jodies.de/archiv/2004/11/13/recent-comments/)
	 *
	 */
	function hot_topics($postbound=5, $excerptcharbound=60){
		global $wpdb,$tablecomments,$tableposts;
		$query = " SELECT * ".
					" FROM $tablecomments c, $tableposts p".
					" WHERE p.ID=c.comment_post_ID".
					" AND post_status = 'publish' AND comment_approved= '1' AND post_password = ''".
					" ORDER BY c.comment_date DESC, p.ID DESC".
					" LIMIT $postbound";

		$formatpost = '<dt><a href="%post_link" title="%post_date">%post_title</a></dt>';
		$formatcomment = '<dd>%comment_excerpt (<a href="%comment_link" title="%comment_date">%comment_author</a>)</dd>';

		$comments = $wpdb->get_results($query);

		$aTmp = array();

		if ($comments) {
			foreach ($comments as $comment) {
				$comment_excerpt = strip_tags($comment->comment_content);
				$comment_excerpt = preg_replace("/[\n\t\r]/"," ",$comment_excerpt);
				$comment_excerpt = preg_replace("/\s{2}/"," ",$comment_excerpt);
				$comment_excerpt = preg_replace("/([^\s]{30})/","$1 ",$comment_excerpt);
				$word = strtok($comment_excerpt," ");
				$comment_excerpt = "";
				while ($word) {
					if (strlen($comment_excerpt . " $word") >
		                            $excerptcharbound) {
						$comment_excerpt = trim($comment_excerpt);
						$comment_excerpt .= "...";
						break;
					}
					$comment_excerpt .= " $word";
					$word = strtok(" ");
				}

				$arrkey = $comment->comment_post_ID . "";

				if (!isset($aTmp[$arrkey])) {
					$post_link    = get_permalink($comment->comment_post_ID);
					$post = get_postdata($comment->comment_post_ID);
					$post_date = mysql2date(get_settings('date_format'),$post['Date']);
					$post_title = $post['Title'];

					$tmp = $comment->comment_post_ID;

					$output = $formatpost . $formatcomment;

					$aTmp[$arrkey] = "";
				} else {
					$output = $formatcomment;
				}

				$comment_link = $post_link . "#comment-$comment->comment_ID";
				$comment_date = mysql2date(get_settings('date_format'),$comment->comment_date);
				$comment_author = $comment->comment_author;
				if (! $comment_author) $comment_author = 'Anonymous';

				$output = preg_replace("/%comment_excerpt/", $comment_excerpt,$output);
				$output = preg_replace("/%comment_link/",    $comment_link,   $output);
				$output = preg_replace("/%comment_author/",  $comment_author, $output);
				$output = preg_replace("/%comment_date/",    $comment_date,   $output);

				$output = preg_replace("/%post_link/",       $post_link,      $output);
				$output = preg_replace("/%post_date/",       $post_date,      $output);
				$output = preg_replace("/%post_title/",      $post_title,     $output);

				$aTmp[$arrkey] .= $output;
			}
		}

		echo "\n" . implode("", $aTmp) . "\n";
	}


	function list_pages($args = '') {
		parse_str($args, $r);
		if (!isset($r['depth'])) $r['depth'] = 0;
		if (!isset($r['show_date'])) $r['show_date'] = '';
		if (!isset($r['child_of'])) $r['child_of'] = 0;
		if ( !isset($r['title_li']) ) $r['title_li'] = __('Pages');


		// Query pages.
		$pages = get_pages($args);
		if ( $pages ) :

		if ( $r['title_li'] )
			echo $r['title_li'] . '<ul>';
		// Now loop over all pages that were selected
		$page_tree = Array();
		foreach($pages as $page) {
			// set the title for the current page
			$page_tree[$page->ID]['title'] = $page->post_title;
			$page_tree[$page->ID]['name'] = $page->post_name;

			// set the selected date for the current page
			// depending on the query arguments this is either
			// the createtion date or the modification date
			// as a unix timestamp. It will also always be in the
			// ts field.
			if (! empty($r['show_date'])) {
				if ('modified' == $r['show_date'])
					$page_tree[$page->ID]['ts'] = $page->time_modified;
				else
					$page_tree[$page->ID]['ts'] = $page->time_created;
			}

			// The tricky bit!!
			// Using the parent ID of the current page as the
			// array index we set the curent page as a child of that page.
			// We can now start looping over the $page_tree array
			// with any ID which will output the page links from that ID downwards.
			$page_tree[$page->post_parent]['children'][] = $page->ID;
		}
		// Output of the pages starting with child_of as the root ID.
		// child_of defaults to 0 if not supplied in the query.
		_page_level_out($r['child_of'],$page_tree, $r);
		if ( $r['title_li'] )
			echo '</ul>';
		endif;
	}



	/*
	 *	method: time_since
	 *
	 *	This plugin is based on code from Dunstan Orchard's Blog. Pluginiffied by Michael Heilemann:
	 *	http://www.1976design.com/blog/archive/2004/07/23/redesign-time-presentation/
	 *
	 *	adapted from original code by Natalie Downe
	 *	http://blog.natbat.co.uk/archive/2003/Jun/14/time_since
	 *
	 *	bugfixed for proper operation, independent of the server's time zone: *_gmt dates must be sent in
	 */
	function time_since($older_date, $newer_date = false)
		{
		// array of time period chunks
		$chunks = array(
		array(60 * 60 * 24 * 365 , 'year'),
		array(60 * 60 * 24 * 30 , 'month'),
		array(60 * 60 * 24 * 7, 'week'),
		array(60 * 60 * 24 , 'day'),
		array(60 * 60 , 'hour'),
		array(60 , 'minute'),
		);

		//	aleck: get gmt offset and update time
		$curtime = time();

		echo "<!-- '$curtime' -->";

		$offset = date("O");
		echo "<!-- '$offset -->";
		if ($offset != "+0000") {
			if (strstr($offset, "-")) {
				$offset = (integer) str_replace("-", "", str_replace("0", "", $offset));
				$curtime = $curtime + (60*60*$offset);
			} elseif (strstr($offset, "+")) {
				$offset = (integer) str_replace("+", "", str_replace("0", "", $offset));
				$curtime = $curtime - (60*60*$offset);
			}
		}
		echo "<!-- '$curtime -->";

		$newer_date = ($newer_date == false) ? $curtime : $newer_date;

		// difference in seconds
		$since = $newer_date - $older_date;

		// we only want to output two chunks of time here, eg:
		// x years, xx months
		// x days, xx hours
		// so there's only two bits of calculation below:

		// step one: the first chunk
		for ($i = 0, $j = count($chunks); $i < $j; $i++)
			{
			$seconds = $chunks[$i][0];
			$name = $chunks[$i][1];

			// finding the biggest chunk (if the chunk fits, break)
			if (($count = floor($since / $seconds)) != 0)
				{
				break;
				}
			}

		// set output var
		$output = ($count == 1) ? '1 '.$name : "$count {$name}s";

		// step two: the second chunk
		if ($i + 1 < $j)
			{
			$seconds2 = $chunks[$i + 1][0];
			$name2 = $chunks[$i + 1][1];

			if (($count2 = floor(($since - ($seconds * $count)) / $seconds2)) != 0)
				{
				// add to output var
				$output .= ($count2 == 1) ? ', 1 '.$name2 : ", $count2 {$name2}s";
				}
			}

		return $output;
	}

	/*
	 *	method: time_of_day
	 *
	 *	Plugin Name: Time of day
	 *	Plugin URI: http://www.ifelse.co.uk/code/timeofday.php
	 *	Description: A WP plugin based on <a href="http://1976design.com/blog/archive/2004/07/23/redesign-time-presentation/">Dunstan's Time of Day code</a> that provides a friendlier display of post times. WP 1.2 users should use the older <a href="http://www.ifelse.co.uk/code/timeofday_12.php">plugin</a>.
	 *	Version: 1.0
	 *	Author: Phu Ly & Dunstan Orchard & Michael Heilemann
	 *	Author URI: http://www.ifelse.co.uk
	*/
	function time_of_day($content) {
	   $hour = date('H',strtotime($content));
	   switch($hour)
			{
			case 0:
			case 1:
			case 2:
				$tod = 'in the wee hours';
				break;
			case 3:
			case 4:
			case 5:
			case 6:
				$tod = 'terribly early in the morning';
				break;
			case 7:
			case 8:
			case 9:
				$tod = 'in the early morning';
				break;
			case 10:
				$tod = 'mid-morning';
				break;
			case 11:
				$tod = 'just before lunchtime';
				break;
			case 12:
			case 13:
				$tod = 'around lunchtime';
				break;
			case 14:
				$tod = 'in the early afternoon';
				break;
			case 15:
			case 16:
				$tod = 'mid-afternoon';
				break;
			case 17:
				$tod = 'in the late afternoon';
				break;
			case 18:
			case 19:
				$tod = 'in the early evening';
				break;
			case 20:
			case 21:
				$tod = 'at around evening time';
				break;
			case 22:
				$tod = 'in the late evening';
				break;
			case 23:
				$tod = 'late at night';
				break;
			default:
				$tod = '';
				break;
			}
		return $tod;
	}
}
?>