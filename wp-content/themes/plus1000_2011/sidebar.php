<div id="sidebar">
<a href="<?php echo qtrans_convertURL(site_url()); ?>"><div id="logoimg"></div></a>
<div id="sidebarmenu">
  <?php meo_generateSimpleLanguageSelect('pre_domain'); ?>
  <ul>
    <h2><li class="page_item page-item-70"><a href="http://www.plus1000.ch<?php _e("<!--:en-->/<!--:--><!--:fr-->/fr/<!--:-->"); ?>"><?php _e("<!--:en-->Home<!--:--><!--:fr-->Accueil<!--:-->"); ?></a></li><?php wp_list_pages('exclude=70,34,59,794,1893&title_li=' ); ?></h2>
  </ul>
  <div class="fb-like" data-href="https://www.facebook.com/pages/Alt-1000-High-Altitude-photography-in-the-mountains/148109168579661" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="true" data-send="false"></div>&nbsp;<a href="http://www.twitter.com/Alt1000Festival"><img src="<?php echo get_bloginfo('template_url') . '/images/twitter.' . meo_getCurrentLanguageISOCode() . '.png'; ?>" alt="<?php _e('<!--:en-->Follow Alt1000Festival on Twitter<!--:--><!--:fr-->Suivez Alt1000Festival avec Twitter<!--:-->'); ?>"/></a>
  <div class="catalogue">
	<a href="<?php _e("<!--:en-->http://www.plus1000.ch/alt-1000-picnic/<!--:--><!--:fr-->http://www.plus1000.ch/fr/alt-1000-picnic/<!--:-->"); ?>"><img title="Catalogue" src="<?php echo get_bloginfo('template_url') . '/images/picnic.jpg'; ?>" alt="" width="146" height="165"><br />
	<span class="puce">_</span><?php _e("<!--:en-->Every day of the week<!--:--><!--:fr-->Tous les jours de la semaine<!--:-->"); ?><br /></a>
	<?php _e("<!--:en-->Discover our picnics <a class='sidebar-a-pink' href='http://www.plus1000.ch/alt-1000-picnic/'>here</a><!--:--><!--:fr-->D&eacute;couvrez nos piques-niques <a class='sidebar-a-pink' href='http://www.plus1000.ch/fr/alt-1000-picnic/'>ici</a><!--:-->"); ?>
  </div>
</div>
</div>



