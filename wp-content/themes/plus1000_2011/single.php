<?php get_header(); ?>

<?php get_sidebar(); ?>

	<div id="content" class="narrowcolumn">

<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<b>single.php</b><hr />
<?php
}
?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="post" id="post-<?php the_ID(); ?>">
			<h2><a href="<?php echo get_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link:'); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>

			<div class="entrytext">
				<?php the_content('<p class="serif">'.__('Read the rest of this entry &raquo;').'</p>'); ?>
				<?php if (function_exists('gengo_link_pages')) : ?>
					<?php gengo_link_pages('<p><strong>'.__('Pages:').'</strong> ', '</p>', 'number'); ?>
				<?php else : ?>
					<?php link_pages('<p><strong>'.__('Pages:').'</strong> ', '</p>', 'number'); ?>
				<?php endif; ?>

			</div>
		</div>
		
		<?php echo meo_getShareLinks($permalink); ?>

	<?php endwhile; else: ?>

		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

<?php endif; ?>

	</div>

<?php get_footer(); ?>
