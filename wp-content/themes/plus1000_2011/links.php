<?php
/*
Template Name: Links
*/
?>

<?php get_header(); ?>

<?php get_sidebar(); ?>

<div id="content" class="narrowcolumn">

<?php
if ( defined('MEO_DEBUG') && MEO_DEBUG ) {
?>
	<b>links.php</b><hr />
<?php
}
?>


<h2><?php _e("Links"); ?>:</h2>
<ul>
<?php get_links_list(); ?>
</ul>

</div>

<?php get_footer(); ?>
